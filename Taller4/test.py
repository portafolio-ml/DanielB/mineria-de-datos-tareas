from pysentimiento import create_analyzer
analyzer = create_analyzer(task="sentiment", lang="es")

print(analyzer.predict("Qué gran jugador es Messi"))
# returns AnalyzerOutput(output=POS, probas={POS: 0.998, NEG: 0.002, NEU: 0.000})
